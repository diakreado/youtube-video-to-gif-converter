$(function() {
    
    // registration
    $(".convert-button").on("click", function(e) {
        e.preventDefault();

        var data = {
            url: $("#url").val(),
            start: $("#start").val(),
            duration: $("#duration").val()
        };
        
        $.ajax({
            type: "POST",
            data: JSON.stringify(data),
            contentType: "application/json",
            url: "/"
          }).done(function(data) {
            if (data.ok) {
                location.reload();
            } else {
                console.error(data);
            }
          });
      
    });
  
});
  