const fs = require('fs');
const youtubedl = require('youtube-dl');
const gify = require('gify');

/** @function
 *  @name downloadAndConvert
 *
 *  Download video form URL and converts it to the GIF
 *
 *  @async
 * 
 *  @example const fileName = downloadAndConvert(https://www.youtube.com/watch?v=3vMRV8ZXRtA, 0:3, 0:2);
 *  
 *  @param {string} url - url of the youtube video
 *  @param {string} start - start time for converting
 *  @param {string} duration - duration of the part of video which shoud be convert to the GIF
 * 
 *  @returns {string} fileName - name of the file
 */
async function downloadAndConvert(url, start, duration) {
    const fileName = 'media/' + Date.now().toString(16);
    const video = youtubedl(url,['--format=18'],{ cwd: __dirname });
    video.pipe(fs.createWriteStream(fileName + '.mp4'));

    video.on('info', function(info) {
        gify(fileName + '.mp4', fileName + '.gif', {start, duration}, function(err){
            if (err) throw err;
        });
    });

    return fileName;
}

module.exports = downloadAndConvert;
