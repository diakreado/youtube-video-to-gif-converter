# Youtube video to gif converter

Turn Youtube videos into gifs.

![](pics/preview.gif)

### Dependencies: 
------
* [gify](https://www.npmjs.com/package/gify)
* [youtube-dl](https://www.npmjs.com/package/youtube-dl)

### Installation
------

#### docker

```sh
$ cd app
$ npm run docker:build
$ npm run docker:run
```

Verify the deployment by navigating to your server address.
(Use your favorite browser)

```sh
127.0.0.1:80
```

#### local server
```sh
$ cd app
$ npm install
$ npm start
```


Verify the deployment by navigating to your server address. 
(Use your favorite browser)

```sh
127.0.0.1:3000
```

### Example
------
![](pics/example.png)

